import axios from 'axios';
import { vatApi } from './constants';

const axiosVAT = axios.create({
    baseURL: vatApi
});

export default axiosVAT;
