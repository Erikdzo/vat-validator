import React from 'react';
import { Row, Col, Container } from 'react-bootstrap';
import VATValidationForm from './components/VATValidationForm';
import VATResult from './components/VATResult';
import ErrorAlert from './components/ErrorAlert';
import axiosVAT from './utils/axiosVAT';

const App = () => {

  const [response, setResponse] = React.useState(null);
  const [error, setError] = React.useState(null);

  const checkVAT = (country, digits) => axiosVAT.get(`numbers?vatNumber=${country}${digits}`)
      .then((res) => {
        setError(null);
        setResponse(res.data);
      })
      .catch((err) => {
        setError(err);
        setResponse(null);
      });


  return (
    <div className="App">
      <Container>
        <Row>
          <Col className="p-5 d-flex justify-content-center">
            <h1>VAT validator</h1>
          </Col>
        </Row>
        <Row>
          <Col className="d-flex justify-content-center">
            <VATValidationForm checkVAT={checkVAT} />
          </Col>
        </Row>
        <Row>
          <Col className="d-flex justify-content-center my-2">
            {error &&
              <ErrorAlert error={error} />
            }
          </Col>
        </Row>
        <Row>
          <Col className="d-flex justify-content-center my-2">
            {response &&
              <VATResult result={response} />
            }
          </Col>
        </Row>
      </Container>
    </div>
  );
}

export default App;
