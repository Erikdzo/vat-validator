import React from 'react';
import PropTypes from 'prop-types';
import { Card, Table } from 'react-bootstrap';

const VATResult = (props) => {

    const { result } = props;

    return (
        <Card className="col-lg-8 px-0">
            <Card.Body>
                <Card.Subtitle className="mb-2 text-muted">
                    {result.RequestDate.substring(0, 10)}
                </Card.Subtitle>

                <Card.Title className={result.Valid ? "text-success" : "text-danger"}>
                    <b>{result.Valid ? "Valid" : "Invalid"}</b>
                </Card.Title>

                <Table className="w-100" style={{ tableLayout: "fixed" }} borderless size="sm">
                    <tbody>
                        <tr className="d-flex">
                            <td className="col-sm-2 col-4 align-self-center"><b>VAT number</b></td>
                            <td className="col-sm-10 col-8 align-self-center">{`${result.CountryCode}${result.VATNumber}`}</td>
                        </tr>
                        <tr className="d-flex">
                            <td className="col-sm-2 col-4 align-self-center"><b>Name</b></td>
                            <td className="col-sm-10 col-8 align-self-center">{result.Name}</td>
                        </tr>
                        <tr className="d-flex">
                            <td className="col-sm-2 col-4 align-self-center"><b>Address</b></td>
                            <td className="col-sm-10 col-8 align-self-center">{result.Address}</td>
                        </tr>
                    </tbody>
                </Table>
            </Card.Body>
        </Card>
    )
}

VATResult.propTypes = {
    result: PropTypes.object.isRequired
};

export default VATResult;
