import React from 'react';
import PropTypes from 'prop-types';
import { Form, Button, Col, Spinner } from 'react-bootstrap';
import { countryCodes } from '../utils/constants';

const VATValidationForm = (props) => {

    const [querying, setQuerying] = React.useState(false);
    const [digits, setDigits] = React.useState("");
    const [country, setCountry] = React.useState("");

    const { checkVAT } = props;

    const handleSubmit = (e) => {
        e.preventDefault();

        setQuerying(true);
        
        checkVAT(country, digits).then(() => {
            setQuerying(false);
        });
    };

    return (
        <Form className="pb-2" onSubmit={handleSubmit}>

            <Form.Label className="d-flex justify-content-center">Enter a VAT number</Form.Label>

            <Form.Row>
                <Form.Group as={Col} sm="auto" controlId="formCountryCode">
                    <Form.Text className="text-muted">Country</Form.Text>
                    <Form.Control
                        required
                        as="select"
                        onChange={e => setCountry(e.target.value)}
                    >
                        <option hidden></option>
                        {countryCodes.map(code =>
                            <option key={`option-${code}`} value={code}>{code}</option>
                        )}
                    </Form.Control>
                </Form.Group>

                <Form.Group as={Col} sm="auto" controlId="formVATDigits">
                    <Form.Text className="text-muted">Code</Form.Text>
                    <Form.Control
                        required
                        type="text"
                        onChange={e => setDigits(e.target.value)}
                        placeholder="etc. 999999999"
                    />
                </Form.Group>
            </Form.Row>

            <Button className="d-flex align-items-center mx-auto" type="submit" disabled={querying}>
                <div>Check VAT number</div>
                {querying &&
                    <Spinner
                        className="ml-2"
                        as="span"
                        animation="border"
                        size="sm"
                        role="status"
                        aria-hidden="true"
                    />
                }
            </Button>
        </Form>
    );
}

VATValidationForm.propTypes = {
    checkVAT: PropTypes.func.isRequired
};

export default VATValidationForm;
