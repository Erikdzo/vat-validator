import React from 'react';
import PropTypes from 'prop-types';
import { Alert } from 'react-bootstrap';

const ErrorAlert = (props) => {

    const { error } = props;

    return (
        <Alert className="col-lg-8" variant="danger" >
            <Alert.Heading>{error.name}</Alert.Heading>
            <p>{error.message}</p>
        </Alert>
    )
}

ErrorAlert.propTypes = {
    error: PropTypes.object.isRequired
};

export default ErrorAlert;
